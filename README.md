webcam的http服务器
=================

## 依赖

- clang++
- opencv
- go

## 编译

> make

## 运行

> ./server 0 :8000 # 在http://ip:8000上运行

浏览器打开http://ip:8000就可以看到图片

## 感谢

[VMXwebcam](https://github.com/VISIONAI/VMXwebcam), MIT licensed