all: server

server: webcam server.go
	go build server.go

webcam: webcam.cc
	clang++ `pkg-config --libs --cflags opencv` $^ -o $@

clean:
	rm -rf webcam server
